package com.epam.rd.java.basic.task8.controller;

public class Flower {
    private String name;
    private Soil soil;
    private String origin;

    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private Multiplaying multiplaying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Soil getSoil() {
        return soil;
    }

    public void setSoil(Soil soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public Multiplaying getMultiplying() {
        return multiplaying;
    }

    public void setMultiplaying(Multiplaying multiplaying) {
        this.multiplaying = multiplaying;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    enum Soil {
        PODZOLISTAYA("подзолистая"),
        DERNOVO_PODZOLISTAYA("дерново-подзолистая"),
        GRUNTOVAYA("грунтовая");

        String name;

        Soil(String name) {
            this.name = name;
        }

        static Soil soilOf(String value){
            for (Soil soil : values()) {
                if (soil.getName().equalsIgnoreCase(value))
                    return soil;
            }
            throw new IllegalArgumentException();
        }


        public String getName() {
            return this.name;
        }
    }
}