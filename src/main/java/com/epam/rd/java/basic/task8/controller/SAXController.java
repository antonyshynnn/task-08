package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private String xmlFileName;

	private List<Flower> flowers;
	private String content;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		this.content =  new String(ch, start, length);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch (qName) {
			case "flowers":
				flowers = new ArrayList<>();
				break;
			case "flower":
				flowers.add(new Flower());
				break;
			case "visualParameters":
				getCurrentFlower().setVisualParameters(new VisualParameters());
				break;
			case "growingTips":
				getCurrentFlower().setGrowingTips(new GrowingTips());
				break;
			case "lighting":
				getCurrentFlower().getGrowingTips().setLighting(GrowingTips.Lighting.valueOf(attributes.getValue("lightRequiring")));
			case "multiplaying":
				getCurrentFlower().setMultiplaying(new Multiplaying());
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
			case "name":
				getCurrentFlower().setName(content);
				break;
			case "soil":
				getCurrentFlower().setSoil(Flower.Soil.soilOf(content));
				break;
			case "origin":
				getCurrentFlower().setOrigin(content);
				break;
			case "stemColour":
				getCurrentFlower().getVisualParameters().setStemColour(content);
				break;
			case "leafColour":
				getCurrentFlower().getVisualParameters().setLeafColour(content);
				break;
			case "aveLenFlower":
				getCurrentFlower().getVisualParameters().setAveLenFlower(Integer.parseInt(content));
				break;
			case "tempreture":
				getCurrentFlower().getGrowingTips().setTemperature(Integer.parseInt(content));
				break;
			case "watering":
				getCurrentFlower().getGrowingTips().setWatering(Integer.parseInt(content));
				break;
			case "multiplying":
				getCurrentFlower().getMultiplying().setMultiplaying(Multiplaying.MultiValues.multiValueOf(content));
				break;

		}
	}

	public Flower getCurrentFlower() {
		return flowers.get(flowers.size() - 1);
	}

	public List<Flower> getFlowers() {
		return flowers;
	}
}