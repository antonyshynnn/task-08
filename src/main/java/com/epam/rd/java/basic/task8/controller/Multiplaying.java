package com.epam.rd.java.basic.task8.controller;

public class Multiplaying {
    private MultiValues multiplaying;

    public MultiValues getMultiplaying() {
        return multiplaying;
    }

    public void setMultiplaying(MultiValues multiplaying) {
        this.multiplaying = multiplaying;
    }

    enum MultiValues {
        LISTYA("листья"),
		CHERENKI("черенки"),
		SEMENA("семена");

        static Multiplaying.MultiValues multiValueOf(String value){
            for (Multiplaying.MultiValues multiValues : values()) {
                if (multiValues.getName().equalsIgnoreCase(value))
                    return multiValues;
            }
            throw new IllegalArgumentException();
        }

        String name;
        MultiValues(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }
}
