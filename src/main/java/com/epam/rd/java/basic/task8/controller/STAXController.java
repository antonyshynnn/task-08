package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private XMLEventReader reader;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> getFlowersFromXml() throws XMLStreamException {
		loadDocument();
		return parseFromXml();
	}

	public void loadDocument(){
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
		} catch (XMLStreamException | FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
		}
	}

	public List<Flower> parseFromXml() throws XMLStreamException {
		List<Flower> flowers = new ArrayList<>();
		if (reader == null) {
			System.err.println("Document didnt loaded");
			return flowers;
		}
		Flower flower = null;
		while (reader.hasNext()) {

			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				StartElement startElement = nextEvent.asStartElement();
				switch (startElement.getName().getLocalPart()) {
					case TagNames.FLOWER:
						flower = new Flower();
						flower.setVisualParameters(new VisualParameters());
						flower.setGrowingTips(new GrowingTips());
						break;
					case TagNames.NAME:
						nextEvent = reader.nextEvent();
						flower.setName(nextEvent.asCharacters().getData());
						break;
					case TagNames.SOIL:
						nextEvent = reader.nextEvent();
						flower.setSoil(Flower.Soil.soilOf(nextEvent.asCharacters().getData()));
						break;
					case TagNames.ORIGIN:
						nextEvent = reader.nextEvent();
						flower.setOrigin(nextEvent.asCharacters().getData());
						break;
					case TagNames.STEM_COLOUR:
						nextEvent = reader.nextEvent();
						flower.getVisualParameters().setStemColour(nextEvent.asCharacters().getData());
						break;
					case TagNames.LEAF_COLOUR:
						nextEvent = reader.nextEvent();
						flower.getVisualParameters().setLeafColour(nextEvent.asCharacters().getData());
						break;
					case TagNames.AVE_LEN_FLOWER:
						nextEvent = reader.nextEvent();
						flower.getVisualParameters().setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
						break;
					case TagNames.TEMPRETURE:
						nextEvent = reader.nextEvent();
						flower.getGrowingTips().setTemperature(Integer.parseInt(nextEvent.asCharacters().getData()));
						break;
					case TagNames.LIGHTING:
						Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
						if (lightRequiring != null) {
							flower.getGrowingTips().setLighting(GrowingTips.Lighting.valueOf(lightRequiring.getValue()));
						}
						break;
					case TagNames.WATERING:
						nextEvent = reader.nextEvent();
						flower.getGrowingTips().setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
						break;
					case TagNames.MULTIPLYING:
						nextEvent = reader.nextEvent();
						flower.setMultiplaying(new Multiplaying());
						flower.getMultiplying().setMultiplaying(Multiplaying.MultiValues.multiValueOf(nextEvent.asCharacters().getData()));
						break;
				}

				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower") && flower != null) {
						flowers.add(flower);
					}
				}
			}
		}
		return flowers;


	}


}