package com.epam.rd.java.basic.task8.controller;

public class GrowingTips {
    private int temperature;
    private Lighting lighting;
    private int watering;

    public int getTempreture() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }


    enum Lighting {
        yes,
        no;

    }
}