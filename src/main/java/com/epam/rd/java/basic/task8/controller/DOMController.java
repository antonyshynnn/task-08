package com.epam.rd.java.basic.task8.controller;


import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	//private List<Flower> flowers;
	private DocumentBuilder documentBuilder;
	private Document document;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	public List<Flower> getFlowersFromXml() {
		DOMBuilder();
		return buildListFlowers();
	}

	private void DOMBuilder() {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		try {
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	private List<Flower> buildListFlowers() {
		List<Flower> flowers = new ArrayList<>();
		try {
			document = documentBuilder.parse(xmlFileName);
			Element root = document.getDocumentElement();
			NodeList flowersList = root.getElementsByTagName("flower");
			for (int i = 0; i < flowersList.getLength(); i++) {
				Element flowerElement = (Element) flowersList.item(i);
				Flower flower = buildFlower(flowerElement);
				flowers.add(flower);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flowers;
	}

	private Flower buildFlower(Element flowerElement) {
		Flower flower = new Flower();

		flower.setName(flowerElement.getElementsByTagName("name").item(0).getTextContent());
		flower.setSoil(Flower.Soil.soilOf(flowerElement.getElementsByTagName("soil").item(0).getTextContent()));
		flower.setOrigin(flowerElement.getElementsByTagName("origin").item(0).getTextContent());

		flower.setVisualParameters(new VisualParameters());
		flower.getVisualParameters().setStemColour(flowerElement.getElementsByTagName("stemColour").item(0).getTextContent());
		flower.getVisualParameters().setLeafColour(flowerElement.getElementsByTagName("leafColour").item(0).getTextContent());
		flower.getVisualParameters().setAveLenFlower(Integer.parseInt(flowerElement.getElementsByTagName("aveLenFlower").item(0).getTextContent()));

		flower.setGrowingTips(new GrowingTips());
		flower.getGrowingTips().setTemperature(Integer.parseInt(flowerElement.getElementsByTagName("tempreture").item(0).getTextContent()));
		flower.getGrowingTips().setLighting(GrowingTips.Lighting.valueOf(flowerElement.getElementsByTagName("lighting")
				.item(0).getAttributes().item(0).getTextContent()));
		flower.getGrowingTips().setWatering(Integer.parseInt(flowerElement.getElementsByTagName("watering").item(0).getTextContent()));

		flower.setMultiplaying(new Multiplaying());
		flower.getMultiplying().setMultiplaying(Multiplaying.MultiValues.multiValueOf(flowerElement.getElementsByTagName("multiplying").item(0).getTextContent()));

		return flower;
	}

	public void parseToXml(List<Flower> flowers, String pathName){
		document = documentBuilder.newDocument();

		Element rootElement = document.createElement("flowers");

		final Attr xmlns = document.createAttribute("xmlns");
		xmlns.setTextContent("http://www.nure.ua");
		final Attr xsi = document.createAttribute("xmlns:xsi");
		xsi.setTextContent("http://www.w3.org/2001/XMLSchema-instance");
		final Attr scheme = document.createAttribute("xsi:schemaLocation");
		scheme.setTextContent("http://www.nure.ua input.xsd ");

		rootElement.setAttributeNode(xmlns);
		rootElement.setAttributeNode(xsi);
		rootElement.setAttributeNode(scheme);
		document.appendChild(rootElement);

		for (Flower f : flowers) {
			final Element flowerEl = document.createElement("flower");
			rootElement.appendChild(flowerEl);
			final Element nameEl = document.createElement("name");
			final Element soilEl = document.createElement("soil");
			final Element originEl = document.createElement("origin");
			nameEl.setTextContent(f.getName());
			soilEl.setTextContent(f.getSoil().getName());
			originEl.setTextContent(f.getOrigin());

			final Element visualParametersEl = document.createElement("visualParameters");
			final Element stemColour = document.createElement("stemColour");
			final Element leafColour = document.createElement("leafColour");
			final Element aveLenFlower = document.createElement("aveLenFlower");

			final Attr aveLenFlowerMeasure = document.createAttribute("measure");

			aveLenFlowerMeasure.setTextContent("cm");
			stemColour.setTextContent(f.getVisualParameters().getStemColour());
			leafColour.setTextContent(f.getVisualParameters().getLeafColour());
			aveLenFlower.setTextContent(String.valueOf(f.getVisualParameters().getAveLenFlower()));

			visualParametersEl.appendChild(stemColour);
			visualParametersEl.appendChild(leafColour);
			aveLenFlower.setAttributeNode(aveLenFlowerMeasure);
			visualParametersEl.appendChild(aveLenFlower);

			final Element growingTipsEl = document.createElement("growingTips");
			final Element tempreture = document.createElement("tempreture");
			final Attr tempMeasure = document.createAttribute("measure");
			final Attr waterMeasure = document.createAttribute("measure");
			final Element lighting = document.createElement("lighting");
			final Attr lightRequiring = document.createAttribute("lightRequiring");
			final Element watering = document.createElement("watering");

			tempreture.setTextContent(String.valueOf(f.getGrowingTips().getTempreture()));
			lightRequiring.setTextContent(f.getGrowingTips().getLighting().toString());
			watering.setTextContent(String.valueOf(f.getGrowingTips().getWatering()));


			tempMeasure.setTextContent("celcius");
			tempreture.setAttributeNode(tempMeasure);
			growingTipsEl.appendChild(tempreture);

			lighting.setAttributeNode(lightRequiring);
			growingTipsEl.appendChild(lighting);

			waterMeasure.setTextContent("mlPerWeek");
			watering.setAttributeNode(waterMeasure);
			growingTipsEl.appendChild(watering);


			final Element multiplyingEl = document.createElement("multiplying");
			multiplyingEl.setTextContent(f.getMultiplying().getMultiplaying().getName());


			flowerEl.appendChild(nameEl);
			flowerEl.appendChild(soilEl);
			flowerEl.appendChild(originEl);
			flowerEl.appendChild(visualParametersEl);
			flowerEl.appendChild(growingTipsEl);
			flowerEl.appendChild(multiplyingEl);
		}

		try (FileOutputStream output = new FileOutputStream(pathName)) {
			writeXml(output);
		} catch (IOException | TransformerException e) {
			e.printStackTrace();
		}
	}

	private void writeXml(OutputStream output) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(output);

			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}




}
