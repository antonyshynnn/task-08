package com.epam.rd.java.basic.task8.controller;

public class TagNames {
    public static final String FLOWERS = "flowers";
    public static final String FLOWER = "flower";
    public static final String NAME = "name";
    public static final String SOIL = "soil";
    public static final String ORIGIN = "origin";
    public static final String VISUAL_PARAMETERS = "visualParameters";
    public static final String STEM_COLOUR = "stemColour";
    public static final String LEAF_COLOUR = "leafColour";
    public static final String AVE_LEN_FLOWER = "aveLenFlower";
    public static final String GROWING_TIPS = "growingTips";
    public static final String TEMPRETURE = "tempreture";
    public static final String LIGHTING = "lighting";
    public static final String WATERING = "watering";
    public static final String MULTIPLYING = "multiplying";

}
